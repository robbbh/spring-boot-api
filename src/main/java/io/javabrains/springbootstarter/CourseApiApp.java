package io.javabrains.springbootstarter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CourseApiApp {

	public static void main(String[] args) {
		// Calling a static method with the class as an argument
		SpringApplication.run(CourseApiApp.class, args);

	}

}
