# Spring Boot Notes #

## What is Spring Boot? ##

Spring Boot makes it easy to create stand-alone, production grade Spring based applications that you can "just run".

## What is Spring? ##

The Spring Framework is an application framework and inversion of control container for the Java platform. The framework's core features can be used by any Java application, but there are extensions for building web applications on top of the Java EE (Enterprise Edition) platform.

* Application Framework
* Programming and Configuration model
* Infrastructure support (connect to different databases)

## Problems with Spring ##

* Huge Framework
* Multiple setup steps 
* Multiple configuration steps
* Multiple build and deploy steps (No best practice/ pathway to do things)

## What Spring Boot gives us ##

* Opinionated 
* Convention over configuration
* Stand alone
* Production ready

---

# Create a Maven project #

## Starting Spring Boot ##

Using the static method `SpringApplication.run(CourseApiApp.class, args);`

* Sets up default configuration 
* Starts Spring application context
* Performs class path scan
* Starts Tomcat server

## Adding a controller ##

* A Java class
* Marked with annotations
**Has info about**
* What URL access triggers it?
* What method to run when accessed?

## Spring MVC ##

The Spring Web MVC framework provides Model-View-Controller (MVC) architecture and ready components that can be used to develop flexible and loosely coupled web applications

## How to get the JAR ##

Using the CLI, go to the project directory level containing the `pom.xml` and execute `mvn clean package`. This will create a JAR which can be dockerised.

## Analysing the pom.xml ##

```
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<groupId>io.javabrains.springbootquickstart</groupId>
	<artifactId>course-api</artifactId>
	<version>0.0.1-SNAPSHOT</version>
	<name>Java Brains Course API</name>

    <parent>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-parent</artifactId>
        <version>1.5.9.RELEASE</version>
    </parent>

    <dependencies>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>
    </dependencies>

    <properties>
        <java.version>1.8</java.version>
    </properties>

</project>
```

The parent tags without the dependencies will mean there are currently no maven dependencies. The parent only pulls the configuration which specifies which JARs to pull when we do specify a dependency. 

The dependency section tells me which JARs to download whereas the parent section tells to me what versions of the JARs to download.

## Starting a Spring Boot App ##

Many ways to start your spring project. These can autogen your pom and boiler plate stuff that we did manually in the tutorial. 

* **[Spring initializr](https://start.spring.io/)**
  * Use Spring version 1.5+.
  * Group = io.javabrains.springbootquickstart.
  * Artifact = course-api.
  * Use GUI to tick Web dependency.
  * **Generate** the project.
  * Change the description to whatever I want.
  * Will download a ZIP which I will need to **import** as an existing maven project into my STS IDE.

Or we can use the **STS IDE**;

* Spring Tool Suite IDE
  * Right click on package manager and create a new spring starter project.
  * Can sort out the configuration using the initialiser.
  * Creates the ZIP file and automatically imports it into the IDE.

## Customising Spring Boot ##

Can have 20% scenarios where Spring Boot needs to be customised.

`application.properties` - Excellent way of specifying custom config.

**Example Usage** 
Can edit the port of the servlet container from the default 8080.

* Create a new file in the resources folder of the project called `application.properties`.

Enter the following;

```
server.port=8081
```

To change more of the configuration search `common application properties` or following **[this](https://docs.spring.io/spring-boot/docs/current/reference/html/common-application-properties.html)** link. Look at the different properties within this file to change.

## Starting with JPA ##

